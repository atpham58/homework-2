% An Pham
% Econ 512
% Homework 2
% General comment from TA: next time use the same repository, but create a
% new folder incide it so that I dont have to clone directory again
function homework2
%% Set question_switch:
% question_switch = 5; Switch to view each question only, if want to.

for question_switch = 1:5
    if question_switch == 1
        %% Question 1
        J = 3; % 1 is A, 2 is B and 3 is C.
        v = -1*ones(J,1);
        p = ones(J,1);

        % Quantity demanded in each case:
        denom = 1 + exp(v(1)-p(1)) + exp(v(2)-p(2)) + exp(v(3)-p(3));
        q_A = exp(v(1)-p(1))/denom; 
        q_B = exp(v(2)-p(2))/denom;
        q_C = exp(v(3)-p(3))/denom;
        q_0 = 1/denom;
        q_TOT = [q_A;q_B;q_C;q_0];

        % Results:
        disp('Question 1: the demand for each option is')
        disp(q_TOT)

    elseif question_switch == 2 
    %% Question 2: Use Broyden to solve for equilibrium
        % Initial values
        for part_switch = 1:4
            if part_switch ==1 % part a
                p0 = [1,1,1]; 
                part='a';
            elseif part_switch ==2 % part b
                p0 = [0,0,0];
                part='b';
            elseif part_switch ==3 % part c
                p0 = [0,1,2];
                part='c';
            elseif part_switch ==4 % part d
                p0 = [3,2,1];
                part='d';
            end

        tic    
        % Use Broyden to solve for equilibrium
        [p_star_broyden,~,flag] = broyden('f',p0');
        toc;

        % Result:
        Q2 = sprintf('Question 2: optimal level of p for each company, part %s',part);
        disp(Q2)
        disp(p_star_broyden) 
            if flag == 0 
                disp('successfully converges');
            else
                 disp('fail to converge');
            end
        end


    elseif question_switch == 3 
    %%  Question 3: Use Gseidel to solve for equilibrium
        J = 3; % 1 is A, 2 is B and 3 is C.
        v = -1*ones(J,1);
        p = ones(J,1);

        % Quantity demanded in each case:
        denom = 1 + exp(v(1)-p(1)) + exp(v(2)-p(2)) + exp(v(3)-p(3));
        q_A = exp(v(1)-p(1))/denom; 
        q_B = exp(v(2)-p(2))/denom;
        q_C = exp(v(3)-p(3))/denom;

        % Initial values
        for part_switch = 1:4
            if part_switch ==1 % part a
                x = [1;1;1]; 
                part='a';
            elseif part_switch ==2 % part b
                x = [0;0;0];
                part='b';
            elseif part_switch ==3 % part c
                x = [0;1;2];
                part='c';
            elseif part_switch ==4 % part d
                x = [3;2;1];
                part='d';
            end

        % Define matrix A: 
        A = diag([q_A,q_B,q_C]-1);

        % Define vector b:
        b = -1*ones(3,1);

        tic
        % Use Gseidel to solve for equilibrium:
        [p_star_gseidel,flag] = gseidel(A,b,x);
        toc;
% comment from TA: you are using gsidel function by Matlab that is solving
% linear system, but you have nonlinear system of equations. See answer
% key.
        % Result:
        Q3 = sprintf('Question 3: optimal level of p for each company, part %s',part);
        disp(Q3)
        disp(p_star_gseidel) 
            if flag == 0 
                disp('successfully converges');
            else
                 disp('fail to converge');
            end
        end

        disp('This method is faster than Broyden');

    elseif question_switch == 4
    %% Question 4
    % Initial values
        for part_switch = 1:4
            if part_switch ==1 % part a
                p0 = [1,1,1]; 
                part='a';
            elseif part_switch ==2 % part b
                p0 = [0,0,0];
                part='b';
            elseif part_switch ==3 % part c
                p0 = [0,1,2];
                part='c';
            elseif part_switch ==4 % part d
                p0 = [3,2,1];
                part='d';
            end
% Comment from TA: here you got the problem wrong. You needed to use
% iterative procedure instead of optimization to find fixed point of it.
% See answer key. 
        options = optimset('Diagnostics','on','MaxFunEvals',15000000, 'MaxIter',100000,'TolFun', 0.00001);
        [p_star_4,~,exitflag] = fsolve(@(p)f4(p),p0,options);
        Q4 = sprintf('Question 4: optimal level of p for each company, part %s',part);
        disp(Q4)
        disp(p_star_4) 
            if exitflag == 1 
                disp('successfully converges');
            else
                 disp('fail to converge');
            end    
        end

        disp('Only part a and part d converges when I set the problem this way. This is because the algorithm is sensitive to initial values');

    elseif question_switch == 5 
        %% Question 5:
        
%         Comment from TA: this pistures are wrong because you are using
%         linear approximation to your problem. 
        J = 3;
        v_A = -1;
        v_B = -1;
        v_C = [-4,-3,-2,-1, 0, 1];
        p = ones(J,1);
        v = [repmat([v_A;v_B],[1,length(v_C)]);v_C];

        % Intitial values:
        x5_1 = [1;1;1];             
        x5_2 = [0;0;0];          
        x5_3 = [0;1;2];           
        x5_4 = [3;2;1];

        L = size(v,2);
        for l = 1:L    
            denom = 1 + exp(v(1,l)-p(1)) + exp(v(2,l)-p(2)) + exp(v(3,l)-p(3));
            q_A = exp(v(1,l)-p(1))/denom; 
            q_B = exp(v(2,l)-p(2))/denom;
            q_C = exp(v(3,l)-p(3))/denom;

            % Define matrix A: 
            A5 = diag([q_A,q_B,q_C]-1);

            % Define vector b:
            b5 = -1*ones(3,1);

            tic
            % Use Gseidel to solve for equilibrium:              
            [p_star_5_1(:,l),flag_1] = gseidel(A5,b5,x5_1);
            [p_star_5_2(:,l),flag_2] = gseidel(A5,b5,x5_2);
            [p_star_5_3(:,l),flag_3] = gseidel(A5,b5,x5_3);
            [p_star_5_4(:,l),flag_4] = gseidel(A5,b5,x5_4);
            toc;            
        end

        % Result:
        % part a:
        Q5_1 = sprintf('Question 5: optimal level of p for each company, part a');
        disp(Q5_1)
        disp(p_star_5_1) 
        if flag_1 == 0 
            disp('successfully converges');
        else
             disp('fail to converge');
        end           
        averageTime_1 = toc/L;
        Avg_time_1 = sprintf('Average Elapsed Time, part a: %6d.', averageTime_1);
        disp(Avg_time_1)

        figure;
        plot(p_star_5_1(1,:),v_C,p_star_5_1(3,:),v_C);
        xlabel('p');
        ylabel('v_C');
        legend('p_A','p_B','Location','northeast');
        title('p vs. v_C, part a');

        % part b:
        Q5_2 = sprintf('Question 5: optimal level of p for each company, part b');
        disp(Q5_2)
        disp(p_star_5_2) 
        if flag_2 == 0 
            disp('successfully converges');
        else
             disp('fail to converge');
        end           
        averageTime_2 = toc/L;
        Avg_time_2 = sprintf('Average Elapsed Time, part b: %6d.', averageTime_2);
        disp(Avg_time_2)

        figure;
        plot(p_star_5_2(1,:),v_C,p_star_5_2(3,:),v_C);
        xlabel('p');
        ylabel('v_C');
        legend('p_A','p_B','Location','northeast');
        title('p vs. v_C, part b');

        % part c:
        Q5_3 = sprintf('Question 5: optimal level of p for each company, part c');
        disp(Q5_3)
        disp(p_star_5_3) 
        if flag_3 == 0 
            disp('successfully converges');
        else
             disp('fail to converge');
        end           
        averageTime_3 = toc/L;
        Avg_time_3 = sprintf('Average Elapsed Time, part c: %6d.', averageTime_3);
        disp(Avg_time_3)

        figure;
        plot(p_star_5_3(1,:),v_C,p_star_5_3(3,:),v_C);
        xlabel('p');
        ylabel('v_C');
        legend('p_A','p_B','Location','northeast');
        title('p vs. v_C, part c');

        % part d:
        Q5_4 = sprintf('Question 5: optimal level of p for each company, part d');
        disp(Q5_4)
        disp(p_star_5_4) 
        if flag_4 == 0 
            disp('successfully converges');
        else
             disp('fail to converge');
        end           
        averageTime_4 = toc/L;
        Avg_time_4 = sprintf('Average Elapsed Time, part d: %6d.', averageTime_4);
        disp(Avg_time_4)

        figure;
        plot(p_star_5_4(1,:),v_C,p_star_5_4(3,:),v_C);
        xlabel('p');
        ylabel('v_C');
        legend('p_A','p_B','Location','northeast');
        title('p vs. v_C, part d');

    end
end

end


